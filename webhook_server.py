import subprocess
from flask import Flask
app = Flask(__name__)

@app.route('/rs', methods=['GET', 'POST'])
def redeploy_rs():
    subprocess.run(["systemctl", "restart", "resolving"])
    return 'Resolving service was restarted.'

@app.route('/ds', methods=['GET', 'POST'])
def redeploy_ds():
    subprocess.run(["systemctl", "restart", "data"])
    return 'Data service was restarted.'

@app.route('/client', methods=['GET', 'POST'])
def redeploy_client():
    subprocess.run(["systemctl", "restart", "client"])
    return 'Client was restarted.'